



public class Concurso {
	
	public static void main(String[] args) {
		
		System.out.println("*********************START********************");
		Concurso c = new Concurso();
		c.empezar();
		
	}
    
	
	
	public void empezar(){
		Movil m1 = new Movil(1,true);
		Movil m2 = new Movil(2,true);
		Movil m3 = new Movil(3,true);
		
		CableUsb c1 = new CableUsb(1,true);
		CableUsb c2 = new CableUsb(2,true);
		CableUsb c3 = new CableUsb(3,true);
		
		Programador p1 = new Programador("p1", Programador.Estado.ESPERANDO, m1, c1);
		Programador p2 = new Programador("p2", Programador.Estado.ESPERANDO, m2, c2);
		Programador p3 = new Programador("p3", Programador.Estado.ESPERANDO, m3, c3);
		Programador p4 = new Programador("p4", Programador.Estado.ESPERANDO, null, null);
		Programador p5 = new Programador("p5", Programador.Estado.ESPERANDO, null, null);
		Programador p6 = new Programador("p6", Programador.Estado.ESPERANDO, null, null);
		
		new Thread(p1).start();
		new Thread(p2).start();
		new Thread(p3).start();
		new Thread(p4).start();
		new Thread(p5).start();
		new Thread(p6).start();
	}
}

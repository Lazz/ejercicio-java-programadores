
public class Movil {
	private int id;
	private boolean libre;
	
	
	
	public Movil() {
		super();
		libre = true;
		id = 0;
	}

	public Movil(int id, boolean libre) {
		super();
		this.id = id;
		this.libre = libre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public boolean isLibre() {
		return libre;
	}

	public void setLibre(boolean libre) {
		this.libre = libre;
	}

	public void coger(){
		libre = false;			
	}
	
	public void soltar(){
		libre = true;	
	}
	
}

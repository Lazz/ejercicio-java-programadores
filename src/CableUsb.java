
public class CableUsb {
	private int id;
	private boolean libre;
	
	
	
	
	public CableUsb() {
		super();
		libre = true;
		id = 0;
	}

	public CableUsb(int id, boolean libre) {
		super();
		this.id = id;
		this.libre = libre;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
	
	public boolean isLibre() {
		return libre;
	}

	public void setLibre(boolean libre) {
		this.libre = libre;
	}

	public void coger(){
		libre = false;			
	}
	
	public void soltar(){
		libre = true;	
	}
	
}

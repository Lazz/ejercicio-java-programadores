
public class Programador implements Runnable { // Cada programador ser� un hilo
	
	public enum Estado {
		PROGRAMANDO, TESTEANDO, ESPERANDO;
	}
	private String nombre;
	private Estado status;
	private Movil movil;
	private CableUsb cable;
	
	
	@Override
	public void run() {
		// TODO Auto-generated method stub
		try {
			desarrollar();
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}


	public Programador() {
		super();
		nombre = "";
		status = Estado.ESPERANDO;
		movil = null;
		cable = null;
	}


	public Programador(String nombre, Estado status, Movil movil, CableUsb cable) {
		super();
		this.nombre = nombre;
		this.status = status;
		this.movil = movil;
		this.cable = cable;
	}


	public String getNombre() {
		return nombre;
	}


	public void setNombre(String nombre) {
		this.nombre = nombre;
	}


	public Estado getStatus() {
		return status;
	}


	public void setStatus(Estado status) {
		this.status = status;
	}


	public Movil getMovil() {
		return movil;
	}


	public void setMovil(Movil movil) {
		this.movil = movil;
	}


	public CableUsb getCable() {
		return cable;
	}


	public void setCable(CableUsb cable) {
		this.cable = cable;
	}


	@Override
	public String toString() {
		return "Programador [nombre=" + nombre + ", status=" + status
				+ ", movil=" + movil.toString() + ", cable=" + cable.toString() + "]";
	}
	
	
	private void desarrollar() throws Exception{
		boolean probado, tengoRecursos=false;
		programar();
		probado = false;
		while(!probado){
			synchronized (cable) {
				synchronized (movil) {
					tengoRecursos = cogerRecursos(movil, cable);
				}
			}
			if (tengoRecursos){
				probar();
				probado=true;
			soltarRecursos(movil, cable);
			}else esperar();
		}		
	}
	
	private void programar() throws Exception{
		System.out.println(nombre+" --> Empiezo a programar");
		Thread.sleep(2000);
		System.out.println(nombre+" --> Acabo de programar");
	}
	
	private void probar() throws Exception{
		System.out.println(nombre+" --> Empiezo a probar");
		Thread.sleep(2000);
		System.out.println(nombre+" --> Acabo de probar");
	}
	
	private void esperar() throws InterruptedException{
		Thread.sleep(2000);
	}
	private boolean cogerRecursos(Movil m, CableUsb c){
		
		if(movil.isLibre() && cable.isLibre()){
			movil.coger(); 
			cable.coger();
			return true;
		}		
		
		return false;
	}
	
	private void soltarRecursos(Movil m, CableUsb c){
		movil.soltar();
		cable.soltar();
		
	}
}
